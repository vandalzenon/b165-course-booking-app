//  [SECTION]Package and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const courseRoute  = require("./routes/courses")
const userRoute = require("./routes/users")
//  [SECTION]Server Setup
const app = express();
app.use(express.json());
dotenv.config();
const srv = process.env.CONNECTION_STRING
const port = process.env.PORT

//  [SECTION] Application Routes
app.use('/courses',courseRoute);
app.use('/users',userRoute);
//  [SECTION] Database Connect
mongoose.connect(srv);
let dbStatus = mongoose.connection;
dbStatus.once('open', () => console.log('\x1b[36m', `Database connected`));

// [SECTION]Gateway Response
app.listen(port, () => console.log(`\x1b[34m`,`Server is running on port ${port}`));


//  [SECTION]POSTMAN
app.get('/', (req, res) => {
    res.send(`Welcome to Mon Franco's Course Booking App!`)
});



