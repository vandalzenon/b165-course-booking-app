// [SECTION]Dependencies and Modules
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const dotenv = require("dotenv");
const auth = require("./../auth");
const res = require("express/lib/response");


dotenv.config();

let salt = Number(process.env.SALT);

// [SECTION]Funtionality [CREATE]
// fn[signUp]
module.exports.signUp = (data) => {
  let ufirstName = data.firstName;
  let ulastName = data.lastName;
  let uEmail = data.email;
  let uPassword = data.password;
  let ugender = data.gender;
  let umobileNo = data.mobileNumber;

  let userModel = new User({
    firstName: ufirstName,
    lastName: ulastName,
    email: uEmail,
    password: bcrypt.hashSync(uPassword, salt),
    gender: ugender,
    mobileNumber: umobileNo,
  });
  return User.findOne({ email: uEmail }).then((existingUser) => {
    if (existingUser) {
      return `Email has already been used`;
    } else {
      return userModel.save().then((result, saveErr) => {
        if (saveErr) {
          return `error in saving the user`;
        } else {
          return result;
        }
      });
    }
  });
};
// fn[Login]
module.exports.loginUser = (data) => {
  let userEmail = data.email;
  let userPassw = data.password;

  return User.findOne({ email: userEmail }).then((foundUser) => {
    if (foundUser) {
      let userHashPass = foundUser.password;

      return bcrypt.compare(userPassw, userHashPass).then((isMatch, err) => {
        if (err) {
          console.log(err);
        } else if (!isMatch) {
          return `Wrong credentials, Pls try again`;
        } else {
          return  auth.createAccessToken(foundUser)
        }
      });
    } else {
      return `Wrong credentials, Pls try again`;
    }
  });
};
// [SECTION]Funtionality [RETRIEVE]
module.exports.getUSerDetails = (id) =>{
  return User.findById(id).then(foundUser => {
    return foundUser
  }).catch(error => res.send(error))
}

module.exports.getEnrollments = (id) =>{
  return User.findById(id).then(foundUser => {
    return foundUser.enrollments;
  })
}
module.exports.getAllUsers = () =>{
  return User.find({}).then(foundUsers =>{
    return foundUsers;
  })
}
// [SECTION]Funtionality [UPDATE]

module.exports.addCourseToUSer = async (user, newcourseID) => {
  if(user.isAdmin === true){
    return "Action Forbidden"
  }
  let newCourse = {
    enrollments: [
      {
        courseID: newcourseID,
      },
    ],
  };
  let userCourseAdded = await User.findByIdAndUpdate(user.id, newCourse).then((existingUser) => {
    if (existingUser) {
      return existingUser;
    } 
  }).catch(err =>err.message);
  
  let enrolees = {
    enrollees:[{
      userID: userCourseAdded._id
    }]};
  
  return Course.findByIdAndUpdate(newcourseID,{$push: enrolees}).then(result =>{
    return result;
  })

};

// [SECTION]Funtionality [DELETE]
