// [SECTION]Dependencies and Modules
const Course = require('../models/Course');



// [SECTION]Funtionality [CREATE]
 module.exports.createCourse = (info) =>{
    let courseName= info.name;
    let courseDesc= info.description;
    let courseCost= info.price;

    let  newCourse = new Course({
        name: courseName,
        description: courseDesc,
        price: courseCost
    })
    return newCourse.save().then((saveSuccess , saveError)=>{
        if (saveError) {
            return 'Failed to save new document' 
        } else {
           return saveSuccess;
        }

    });
}
// [SECTION]Funtionality [RETRIEVE]
    // Fn[RETRIEVE] all documents 
        module.exports.getAllCourse = () =>{
            return Course.find({}).then(result =>{ return result;})
        }
    // Fn[RETRIEVE] single document 
        module.exports.getCourse = (Id) =>{
        return Course.findById(Id).then(result =>{ return result;})
    }
    // Fn[RETRIEVE] all active document 
        module.exports.getAllActiveCourse = () =>{
        return Course.find({isActive:true}).then(result =>{ return result;})
    }
// [SECTION]Funtionality [UPDATE]
    module.exports.updateCourse = (id,data) => {   
        let putCourse ={
            name: data.name,
            description: data.description,
            price:data.price
        }
        return Course.findByIdAndUpdate(id, putCourse).then((updateSuccess,Err) => {
            if (Err){
                return 'failed to update Course'
            }
            else{
                return `Update was successful`;
            }
        })
    }
    module.exports.deactivateCourse=(id)=>{
        let statusValue = {
            isActive: false
        }
        return Course.findByIdAndUpdate(id, statusValue).then((updateSuccess,Err) => {
            if (Err){
                return 'failed to deactivate Course'
            }
            else{
                return `deactivation of ${id} course was successful`;
            }
        })
    }

    module.exports.reactivateCourse=(id)=>{
        let statusValue = {
            isActive: true
        }
        return Course.findByIdAndUpdate(id, statusValue).then((updateSuccess,Err) => {
            if (Err){
                return 'failed to reactivate Course'
            }
            else{
                return `reactivation of ${id} course was successful`;
            }
        })
    }
// [SECTION]Funtionality [DELETE]

module.exports.deleteCourse = (id) =>{
    return Course.findByIdAndRemove(id).then((removedCourse,deleteErr) =>{
        if(removedCourse){
            return 'course successfully deleted';
        }else{
            return 'no course was removed' 
        }
        })
}