const jwt = require('jsonwebtoken');

const secret = "CourseBookingAPI";


module.exports.createAccessToken = (userData) =>{
    const data = {
        id: userData._id,
        email: userData.email,
        isAdmin: userData.isAdmin
    }

    return jwt.sign(data,secret, {})
}


module.exports.verify = (req,res,next) =>{
    let token = req.headers.authorization;
    if (typeof token === "undefined"){
        return 'forbidden'
    }else{
        token = token.split(" ");
        let accessToken =  token[1];
        jwt.verify(accessToken, secret,(error,decodedToken) =>{
            if (error){
                return res.send('Action Forbidden');
            }else{
                req.user = decodedToken;
                next();
            }
        })
        return 
    }

}
module.exports.verifyAdmin = (req,res,next) =>{
    if(req.user.isAdmin){
        next();
    }else{
        return res.send('Action Forbidden');
    }
}