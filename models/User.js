// *[SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// *[SECTION]



// *[SECTION] Blueprint Schema

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required']
    },
    mobileNumber: {
        type: String,
        required: [true, 'Mobile Number is required']
    },
    gender:{
        type:String,
        required: [true, 'Gender is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    enrollments: [
        {
            courseID:{
            type: String,
            required : [true,'Course ID is required']
            },
            enrollOn:{
            type: Date,
            default: new Date()
            },
            status :{
            type: String,
            default: 'Enrolled'
            }
        }
    ]
   


})

// *[SECTION] Model
const User = mongoose.model('User',userSchema);
module.exports = User;