// *[SECTION] Dependencies and Modules
const mongoose = require('mongoose');



// *[SECTION] Blueprint Schema
let courseSchema = new mongoose.Schema({
 name:{
     type:String,
     required:[true, 'Course Name is Required']
 },
 description:{
     type:String,
     required:[true, 'Course description is Required']
 },
 price:{
     type:Number,
     required:[true, 'Price is Required']
 },
 isActive:{
     type:Boolean,
    default:true
 },
 createOn:{
     type:Date,
     default:new Date()
 },
 enrollees:[{
         userID:{
             type:String,
             required :[true, "User's ID is Required"]
         },
         enrolledOn:{
             type:Date,
             default:new Date()
         }

     }],
     
 }
)
// *[SECTION] Model
const Course = mongoose.model('Course',courseSchema);
module.exports = Course;