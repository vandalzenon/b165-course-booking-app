// [SECTION] Dependencies and Modules
const exp = require("express");
const controller = require("../controllers/courses")

// [SECTION] Routing Component
const route = exp.Router();

// [SECTION] [PST] Routes
route.post('/create', (req, res) => {
    let data = req.body;
    controller.createCourse(data).then(outcome => {
        res.send(outcome);
    })
})

// [SECTION] [GET] Routes
// fn[GET] all documents
route.get('/all', (req, res) => {
    controller.getAllCourse().then(outcome => {
        res.send(outcome)
    })
});
// fn[GET] all active document
route.get('/active', (req, res) => {
    controller.getAllActiveCourse().then(outcome => {
        res.send(outcome)
    })
});
// fn[GET] document by <id>
route.get('/:objectID', (req, res) => {
    let id = req.params.objectID;
    controller.getCourse(id).then(outcome => {
        res.send(outcome)
    })
});

// [SECTION] [PUT] Routes
route.put('/:objectID',(req,res) => {
    let id = req.params.objectID;
    let data = req.body;

    let detailsName = data.name;
    let detailsDesc = data.description;
    let detailsPrice = data.price;

    if (detailsName !== ''&& detailsDesc !== '' && detailsPrice !==''){
        controller.updateCourse(id,data).then(outcome =>{
            res.send(outcome)
        })
    }else{
            res.send(`some data seem to be missing, pls check again`)
    } 
})
route.put('/:objectID/archive',(req, res) => {
    let id = req.params.objectID;
    controller.deactivateCourse(id).then(outcome =>{
        res.send(outcome)
})
})

route.put('/:objectID/reactivate',(req, res) => {
    let id = req.params.objectID;
    controller.reactivateCourse(id).then(outcome =>{
        res.send(outcome)
})
})

// [SECTION] [DEL] Routes
route.delete('/:objectID', (req,res) => {
    let id = req.params.objectID;
    controller.deleteCourse(id).then(outcome => {
        res.send(outcome)
    })
    
})


// [SECTION] Export Routes
module.exports = route;
