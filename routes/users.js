// [SECTION] Dependencies and Modules
const exp = require("express");
const controller = require("../controllers/users")
const {verify,verifyAdmin} = require("./../auth");

//  [SECTION] Routing Component
const route = exp.Router();

// [SECTION] [PST] Routes
route.post('/register', (req, res) => {
    let data = req.body;
    controller.signUp(data).then(result => {
        res.send(result);
    })
})
// fn[login]
route.post('/login', (req,res) => {
    let data = req.body;
    controller.loginUser(data).then(result => {
        res.json(result);
    })
})

// [SECTION] [GET] Routes


// fn[GET] document by <id>
route.get('/getUser',verify, (req,res) => {
    let id = req.user.id;
    controller.getUSerDetails(id).then(result =>{
        res.send(result);
    })
})

route.get('/all',verify,verifyAdmin,(req,res)=>{
    controller.getAllUsers().then(result =>{
        res.send(result);
    })
})

route.get('/enrollment',verify, (req,res)=>{
    let user = req.user.id;
   
    controller.getEnrollments(user).then(result =>{
        res.send(result);
    })
})

// [SECTION] [PUT] Routes
route.put('/enrollCourse',verify, (req,res)=>{
    let user = req.user;
    let courseID = req.body.courseID;
    controller.addCourseToUSer(user,courseID).then(result =>{
        res.send(result);
    })
})



// [SECTION] [DEL] Routes



// [SECTION] Export Routes
module.exports = route;
